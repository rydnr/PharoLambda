#!/usr/bin/env bash
# Bash script to build a Lambda compatible Zip project

set -veu

echo -e "\e[1m\nChecking Pharo VM setup...\e[0m"
if [ ! -e build ]; then
    echo -e "\e[1m\nDidn't detect a built VM...\e[0m";
    exit;
fi

rm -rf deploy
mkdir deploy

echo -e "\e[1m\nCopying Smalltalk project scripts...\e[0m"
cp scripts/*.st build
cd build

ls -lh Pharo.*

if [ "${USE_MIN_IMAGE:-}" = true ]; then
    echo -e "\e[1m\nInstalling Pharo minimal image remote load support...\e[0m";
    ./pharo Pharo.image --no-default-preferences --save  --quit st minimal.st;
fi

ls -lh Pharo.*

echo -e "\e[1m\nConfiguring credentials...\e[0m";
mkdir -p ~/.config/pharo
cat <<EOF > ~/.config/pharo/credentials.st
StartupPreferencesLoader default executeAtomicItems: {
  StartupAction
    name: 'Git Settings'
    code: [
      FileStream stdout cr; nextPutAll: 'Setting the git credentials'; cr.
      Iceberg enableMetacelloIntegration: true.
      IceCredentialStore current
        storeCredential: (IcePlaintextCredentials new 
          username: '${GITLAB_USER_NAME}';
          password: '${GITLAB_USER_PASSWORD}';
          host: 'gitlab.com';
          yourself).
      FileStream stdout cr; nextPutAll: 'Finished'; cr
    ]
}
EOF

echo -e "\e[1m\nInstalling Smalltalk project packages...\e[0m"
./pharo Pharo.image --no-default-preferences --save --quit st loadLocal.st config.st \
    "{'$AWS_ACCESS_KEY_ID'. '$AWS_SECRET_ACCESS_KEY'. '$AWS_DEFAULT_REGION'. '$S3_BUCKET'}" |& tee LoadLocal.log

ls -lh Pharo.*

if (grep -f ../scripts/errorPatterns.txt LoadLocal.log | grep -v "XMLParser"); then
    echo -e "\e[91m\nErrors detected in package load!\n\e[0m";
    exit 1;
fi

echo -e "\e[1m\nCreating Lambda deployment zip...\e[0m"
zip -r --symlinks ../deploy/$LAMBDA_NAME.zip * -x pharo-local/\* \*.sources \*.changes \*.st \*.log \
    */libgit2.* */libSDL2* */B3DAccelerator* */JPEGRead* */vm-sound* */vm-display* \
    */libfreetype.* */libglutin.* tmp/\* github-cache/\* */__MACOSX\*
zip -uR ../deploy/$LAMBDA_NAME.zip *-null.so

cd ..
zip deploy/$LAMBDA_NAME.zip $LAMBDA_NAME.js
cp scripts/deploy.sh deploy
ls -lh deploy

SIZE=$(bc<<<"scale=2; $(unzip -l deploy/$LAMBDA_NAME.zip | tail -1 | xargs | cut -d' ' -f1)/1024/1024")
echo -e "\e[1m\nContents Size $SIZE mb\n\e[0m"
