internal
isDebugRequestFor: json
	
	json
		at: 'request'
		ifPresent: [ :r | r
				at: 'intent'
				ifPresent: [ :i | 
					^(i at: 'name') = 'DebugIntent' ] ].
			
	^json includesKey: 'debug'